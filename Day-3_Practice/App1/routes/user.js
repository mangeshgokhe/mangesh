const express = require('express')
const db = require('../db')
const utils = require('../utils')
const cryptoJs = require('crypto-js')

const router = express.Router()

router.post('/register', (request, response) => {
    const {firstName, lastName, email, password} = request.body
    const encryptedPass=cryptoJs.MD5(password)
    const statement = `insert into user (firstName, lastName, email, password) values ('${firstName}', '${lastName}', '${email}', '${encryptedPass}')`
    db.pool.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.post('/login', (request, response) => {
    const {email, password} = request.body
    const encryptedPass=cryptoJs.MD5(password)
    const statement = `select id, firstName, lastName, email from user where email = '${email}' and password = '${encryptedPass}'`
    db.pool.execute(statement, (error, users) => {

        const result = {}
        if (error) {
            result['status'] = 'error'
            result['error'] = error
        } else {
            if (users.length == 0) {
                // user does not exist
                result['status'] = 'error'
                result['error'] = 'invalid email           or password'
            } else {
                // users exists
                const user = users[0]
                result['status'] = 'success'
                result['data'] = user
            }
        }

        response.send(result)
    })
})

module.exports = router
