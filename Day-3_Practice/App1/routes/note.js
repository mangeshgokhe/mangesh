const express = require('express') 
const db = require('../db')
const utils = require('../utils')
const cryptoJs = require('crypto-js')

const router = express.Router()

router.get('/my/:userId', (request, response) => {
    const id = request.params.userId
    const statement = `select id, contents, status from note where user_id = ${id}`
    db.pool.execute(statement, (error, notes) => {
        response.send(utils.createResult(error, notes))
    })
})

router.post('/', (request, response) => {
    const {user_id, contents} = request.body
    const statement = `insert into note (user_id, contents, status) values (${user_id}, '${contents}', 0)`
    db.pool.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.put('/:noteId', (request, response) => {
    const noteId = request.params.noteId
    const {contents} = request.body
    const statement = `update note set contents = '${contents}' where id = ${noteId}`
    db.pool.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.delete('/:noteId', (request, response) => {
    const noteId = request.params.noteId
    const statement = `delete from note where id = ${noteId}`
    db.pool.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

module.exports = router
