const express = require('express')
//const jwt = require('jsonwebtoken')
// const config = require('./config')
// const utils = require('./utils')

// for swagger
// const swaggerJsDoc = require('swagger-jsdoc')
// const swaggerUi = require('swagger-ui-express')

// get the morgan
const morgan = require('morgan')

// get all the routes
const routerUser = require('./routes/user')
const routerNote = require('./routes/note')

const app = express()
app.use(express.json())
app.use(morgan('combined'))

// for swagger
// const swaggerJsDocOptions = {
//     swaggerDefinition: {
//         info: {
//             title: 'Notes APIs',
//             version: '1.0',
//             description: 'This is notes apis application'
//         }
//     },
//     apis: ['./routes/*.js']
// }

// const jsDocSepc = swaggerJsDoc(swaggerJsDocOptions)
// app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(jsDocSepc))

app.get('/', (request, response) => {
    response.send(`<h1>Welcome to my notes application APIs</h1>`)
})

// check if the token is valid 
// app.use((request, response, next) => {

//     // check the url and see if the token is required
//     if ((request.url == '/user/login') ||
//         (request.url == '/user/register') || 
//         (request.url == '/api-docs'))  {

//         // token is not required
//         next()
//     } else {
//         const token = request.headers['x-auth-token']
//         try {
//             const data = jwt.verify(token, config.secret)
//             const userId = data.id

//             // add the user id to the request
//             request.userId = userId

//             next()
//         } catch(ex) {
//             response.status(401)
//             response.send(utils.createUnauthorizedResult())
//         }
//     }
// })

// add all the routes
app.use('/', routerUser)
app.use('/', routerNote)

app.listen(4000, '0.0.0.0', () => {
    console.log('server started on port 4000')
})
