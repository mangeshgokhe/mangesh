const mysql = require('mysql2')

function createPool() {
    const pool = mysql.createPool({
        host: 'localhost',
        user: 'root',
        password: 'SQL@1234',
        database: 'mean_02_notes',
        port: 3306,
        waitForConnections: true,
        connectionLimit: 100
    })

    return pool
}

// create a pool of 100 connections
const pool = createPool()

module.exports = {
    pool: pool
}
