﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EmployeeDataAccess;

namespace EmployeeWithSQLConnection.Controllers
{
    public class EmployeeController : ApiController
    {
        [HttpGet]
        //public IEnumerable<Employee> Load()
        //{
        //    using(PracticeDatabaseEntities Practice=new PracticeDatabaseEntities())
        //    {
        //        return Practice.Employees.ToList();
        //    }
        //}
        public HttpResponseMessage Load(string gender="All")
        {
            try
            {
                using (PracticeDatabaseEntities Practice = new PracticeDatabaseEntities())
                {
                    switch (gender.ToLower())
                    {
                        case "all":
                            return Request.CreateResponse(HttpStatusCode.OK, Practice.Employees.ToList());
                        case "male":
                            return Request.CreateResponse(HttpStatusCode.OK, Practice.Employees.Where(e => e.Gender.ToLower() == "male").ToList());
                        case "female":
                            return Request.CreateResponse(HttpStatusCode.OK, Practice.Employees.Where(e => e.Gender.ToLower() == "female").ToList());

                        default: return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Value for gender must be all,male,female:" + gender + "is invalid");
                    }
                }
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,ex);
            }
        }

        //public Employee Get(int id)
        //{
        //    using (PracticeDatabaseEntities Practice = new PracticeDatabaseEntities())
        //    {
        //        return Practice.Employees.FirstOrDefault(e =>e.id==id);
        //    }
        //}
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            using(PracticeDatabaseEntities entities =new PracticeDatabaseEntities())
            {
                var entity = entities.Employees.FirstOrDefault(e => e.id == id);
               if(entity!=null)
               {
                   return Request.CreateResponse(HttpStatusCode.OK, entity);
               }
                else
               {
                   return Request.CreateErrorResponse(HttpStatusCode.NotFound,"Employee witth id = "+id.ToString()+" not found");
               }
            }
        }

        public HttpResponseMessage Post([FromBody] Employee Emp)
        {
            try
            {
                using (PracticeDatabaseEntities entity = new PracticeDatabaseEntities())
                {
                    entity.Employees.Add(Emp);
                    entity.SaveChanges();
                    var message = Request.CreateResponse(HttpStatusCode.Created, Emp);
                    message.Headers.Location = new Uri(Request.RequestUri + Emp.id.ToString());
                    return message;
                }
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);   
            }
        }

        //public void Delete(int id)
        //{
        //    using(PracticeDatabaseEntities Deleting =new PracticeDatabaseEntities())
        //    {
        //        Deleting.Employees.Remove(Deleting.Employees.FirstOrDefault(e => e.id == id));
        //        Deleting.SaveChanges();
        //    }
        //}
        [HttpDelete]
        public HttpResponseMessage DeleteEmp(int id)
        {
            try
            {
                using (PracticeDatabaseEntities Deleting = new PracticeDatabaseEntities())
                {

                    var EmpDelete = Deleting.Employees.FirstOrDefault(e => e.id == id);

                    if (EmpDelete == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Employee witth id = " + id.ToString() + " not found For deleting");
                    }
                    else
                    {
                        EmpDelete = Deleting.Employees.Remove(Deleting.Employees.FirstOrDefault(e => e.id == id));
                        Deleting.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, EmpDelete);
                    }
                }
            }
            catch(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage Put([FromBody]int id,[FromUri] Employee Emp)
        {
            try
            {
                using (PracticeDatabaseEntities Editing = new PracticeDatabaseEntities())
                {
                    var EmpEdit = Editing.Employees.FirstOrDefault(e => e.id == id);
                    if (EmpEdit == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Employee witth id = " + id.ToString() + " not found for editing");
                    }
                    else
                    {
                        EmpEdit.FirstName = Emp.FirstName;
                        EmpEdit.LastName = Emp.LastName;
                        EmpEdit.Gender = Emp.Gender;
                        EmpEdit.Salary = Emp.Salary;
                        Editing.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, EmpEdit);
                    }

                }
            }
            catch(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);

            }
        }
    }
}
